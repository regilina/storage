package com.example.demo.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity

@ToString
@Getter
@Setter
@NoArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @NotNull
    private Integer id;
    private String code;
    private String name;
    private String groupp;
    private String count;
    @Column(name = "pass_to_photo")
    private String passToPhoto;
    private String description;

}
