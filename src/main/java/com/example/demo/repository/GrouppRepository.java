package com.example.demo.repository;

import com.example.demo.model.Groupp;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GrouppRepository extends CrudRepository<Groupp, Integer> {
    List<Groupp> findAll();
    List<Groupp> findByName(String name);
}