package com.example.demo.repository;

import com.example.demo.model.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Integer> {
    List<Item> findAllById(Integer id);
    List<Item> findByCodeContains(String search);
    List<Item> findByNameContains(String search);
    List<Item> findByGroupp(String group);

}