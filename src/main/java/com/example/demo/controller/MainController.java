package com.example.demo.controller;

import com.example.demo.model.Groupp;
import com.example.demo.model.Item;
import com.example.demo.repository.GrouppRepository;
import com.example.demo.repository.ItemRepository;
import com.example.demo.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class MainController {
    private final StorageService storageService;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private GrouppRepository grouppRepository;
    @Autowired
    public MainController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping(path={ "/", "/home"})
    public String home (HttpSession session, Model model) {
        session.setAttribute("group", new Groupp("Все"));
        session.setAttribute("items", new ArrayList<Item>());

        model.addAttribute("group", "");
        model.addAttribute("groups", grouppRepository.findAll());
        return "home";
    }

    @GetMapping(path={ "/add"})
    public String add (HttpSession session, Model model) {

        model.addAttribute("group", ((Groupp)session.getAttribute("group")).getName());
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("itemForm", new Item());
        return "add";
    }

    @GetMapping(path={ "/delete"})
    public String delete (HttpSession session, Model model) {
        model.addAttribute("group", ((Groupp)(session.getAttribute("group"))).getName());
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("delete", "on");
        return "index";
    }

    @GetMapping(path={"/allItems"})
    public String allItems (HttpSession session, Model model) {
        Groupp group = (Groupp)session.getAttribute("group");
        group.setName("Все");
        List<Item> items = (List<Item>)session.getAttribute("items");
        items.clear();
        items.addAll((List<Item>) itemRepository.findAll());

        model.addAttribute("group", "Все");
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("items", itemRepository.findAll());
        return "index";
    }

    @GetMapping(path={ "/back"})
    public String back (HttpSession session, Model model) {
        model.addAttribute("group", ((Groupp)(session.getAttribute("group"))).getName());
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("items", session.getAttribute("items"));
        return "index";
    }

    @PostMapping(path="/save")
    public String save (Item item, @RequestParam("file") MultipartFile file, HttpSession session, Model model) {
        if ((grouppRepository.findByName(item.getGroupp()).isEmpty())) {
            grouppRepository.save(new Groupp(item.getGroupp()));
        }
        if(!file.isEmpty()) {
            storageService.store(file);
            item.setPassToPhoto(file.getOriginalFilename());
        } else {
            item.setPassToPhoto("default.png");
        }
        itemRepository.save(item);

        model.addAttribute("group", ((Groupp)(session.getAttribute("group"))).getName());
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("itemForm", new Item());
        model.addAttribute("message", "Элемент " + item.getName() + " добавлен!");

        return "add";
    }

    @PostMapping(path="/deleteGroup")
    public String deleteGroup (String groupToDelete, HttpSession session, Model model) {
        String message = "Группа " + groupToDelete + " удалена!";
        try {
            grouppRepository.delete(grouppRepository.findByName(groupToDelete).get(0));
        } catch (Exception e){
           message = "Что-то пошло не так. Ошибка: 130";
            e.printStackTrace();
        }
        Set<Item> forDelete = new HashSet<>();
        for (Item item : itemRepository.findAll()) {
            if (item.getGroupp().equals(groupToDelete)) {
                forDelete.add(item);
            }
        }
        itemRepository.deleteAll(forDelete);

        model.addAttribute("group",((Groupp)(session.getAttribute("group"))).getName());
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("message", message);
        model.addAttribute("delete", "on");
        return "index";
    }

    @PostMapping(path="/editItem")
    public String editItem (Item item,  @RequestParam("file") MultipartFile file, HttpSession session, Model model) {
        String path;
        if ((grouppRepository.findByName(item.getGroupp()).isEmpty())) {
            grouppRepository.save(new Groupp(item.getGroupp()));
        }
        if(!file.isEmpty()) {
            storageService.store(file);
            path = (file.getOriginalFilename());
        } else {
            path = item.getPassToPhoto();
        }
        List<Item> items = (List<Item>)session.getAttribute("items");
        int i = 0;
        for ( ; i < items.size(); i++) {
            if(items.get(i).getId().equals(item.getId())) {
                break;
            }
        }
        ((List<Item>)(session.getAttribute("items"))).remove(i);
        itemRepository.deleteById(item.getId());
        item.setPassToPhoto(path);
        Item newItem = itemRepository.save(item);
        ((List<Item>)(session.getAttribute("items"))).add( i, newItem);
        model.addAttribute("item", newItem);
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("message", "Изменения сохранены!");
        model.addAttribute("group", ((Groupp)(session.getAttribute("group"))).getName());

        return "item";
    }

    @GetMapping(path="/delete/byId/{id}")
    public String byId (@PathVariable("id") Integer id, HttpSession session , Model model) {
        String message;
        try {
            Item itemToDelete = itemRepository.findAllById(id).get(0);
            itemRepository.deleteById(id);
            message = "Элемент " +  itemToDelete.getName() + " удален!";
            (    (List<Item>)(session.getAttribute("items"))).removeIf(e -> e.getId().equals(id));

       } catch (Exception e) {
            message = "Что-то пошло не так. Ошибка: 120";
            e.printStackTrace();
        }
        model.addAttribute("group",((Groupp)(session.getAttribute("group"))).getName());
        model.addAttribute("message", message);
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("items", session.getAttribute("items"));
        return "index";
    }

}
