package com.example.demo.controller;

import com.example.demo.model.Groupp;
import com.example.demo.model.Item;
import com.example.demo.repository.GrouppRepository;
import com.example.demo.repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller

@RequestMapping(path = "/search")
public class SearchController {
    private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private GrouppRepository grouppRepository;
    private List<Item> resultList;

    @PostMapping(path="/general")
    public String general (@RequestParam("search") String search, HttpSession session, @RequestParam("gr") String gr, Model model) {

        List<Item> items = new ArrayList<>();
        items.addAll(itemRepository.findByNameContains(search));
        items.addAll(itemRepository.findByCodeContains(search));
        Set<Item> tempSet = new HashSet<>(items);
        items.clear();
        items.addAll(tempSet);
        List<Groupp> groups = grouppRepository.findAll();

        if(null == gr || gr.isEmpty() || gr.equals("Все")) {
            session.removeAttribute("group");
            session.setAttribute("group", new Groupp("Все"));
            model.addAttribute("group", "Все");
        }
        else {
            List<Item> itemsss = new ArrayList<>();
            for (Item item : items) {
                if(item.getGroupp().equals(gr)) {
                    itemsss.add(item);
                }
            }
            items = itemsss;
        }
        model.addAttribute("items", items);
        model.addAttribute("group", gr);
        model.addAttribute("groups", groups);
        model.addAttribute("search", search);
        session.setAttribute("items", items);
        return "index";
    }

    @GetMapping(path="/byGroup/{groupName}")
    public String byGroup (@PathVariable String groupName, HttpSession session, Model model) {
        Groupp group = (Groupp)session.getAttribute("group");
        group.setName(groupName);
        List<Item> items = (List<Item>)session.getAttribute("items");
        items.clear();
        items.addAll(itemRepository.findByGroupp(groupName));

        model.addAttribute("items", items);
        model.addAttribute("group", groupName);
        model.addAttribute("groups", grouppRepository.findAll());

        return "index";
    }

    @GetMapping(path="/byId/{id}")
    public String byId (@PathVariable("id") Integer id, HttpSession session, Model model) {
        resultList = itemRepository.findAllById(id);
        model.addAttribute("group", ((Groupp)(session.getAttribute("group"))).getName() );
        model.addAttribute("groups", grouppRepository.findAll());
        model.addAttribute("item", resultList.get(0));
        model.addAttribute("id", id);
        return "item";
    }

}
